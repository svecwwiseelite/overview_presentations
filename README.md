Find all the presentations related to project here.

All the presentations are LaTeX files, some may have styles or bib files separately some may not.

All the images related to the particular presentation are put in images directory in the respective presentations.

To view any presentation download the complete directory and convert the .tex file to pdf format.

Here is the command to convert the LaTeX file to pdf in command line is : **pdflatex filename.tex**
